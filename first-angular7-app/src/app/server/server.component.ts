import { Component } from '@angular/core';

@Component({
    selector: 'app-server',
    templateUrl: './server.component.html'
})

export class ServerComponent {
    serverId = 10;
    serverStatus = 'offline';
    allowNewServer = true;
    serverCreationStatus = 'No new server created';
    serverName = '';
    username = '';
    userNameEmptyString = true;

    getServerStatus(){
        return this.serverStatus;
    }

    onCreateServer(){
        this.serverCreationStatus = 'New Server Created ' + this.serverName;
    }

    onUsernameButtonClick(){
        this.username = '';
    }
}